package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    //@Autowired allows us to use the interface and allow us to use the methods from the CrudRepository
    @Autowired
    private UserRepository userRepository;

    // Create User
    public void createUser(User user) {
        userRepository.save(user);
    }

    // Get Users
    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    // Delete User
    public ResponseEntity deleteUser(Long id) {
        userRepository.deleteById(id);
        return new ResponseEntity<>("User Deleted Successfully", HttpStatus.OK);
    }

    // Update User
    public ResponseEntity updateUser(Long id, User user) {
        User userForUpdating = userRepository.findById(id).get();

        userForUpdating.setUsername(user.getUsername());
        userForUpdating.setPassword(user.getPassword());
        userRepository.save(userForUpdating);
        return new ResponseEntity<>("User Updated Successfully", HttpStatus.OK);

    }

    // Find User by Username
    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepository.findByUsername(username));
    }
}
