package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;


public interface UserService {
    // Create User
    void createUser(User user);

    // View All Posts
    Iterable<User> getUsers();

    // Delete User
    ResponseEntity deleteUser(Long id);

    // Update User
    ResponseEntity updateUser(Long id, User user);

    //Find User by Username
    Optional<User> findByUsername(String username);
}
