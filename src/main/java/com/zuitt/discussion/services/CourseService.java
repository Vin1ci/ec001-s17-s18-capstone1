package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {
    // Create Course
    void createCourse(String stringToken, Course course);

    // View All Courses
    Iterable<Course> getCourses();

    // Delete Course
    ResponseEntity deleteCourse(Long id, String stringToken);

    // Update Courses
    ResponseEntity updateCourse(Long id, String stringToken, Course course);

    // Get All Course of a Specific User
    Iterable<Course> getMyCourses(String stringToken);
}
