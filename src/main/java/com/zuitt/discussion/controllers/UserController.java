package com.zuitt.discussion.controllers;

import com.zuitt.discussion.exceptions.UserException;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin

public class UserController {
    @Autowired
    UserService userService;

    // Create Users
    @RequestMapping(value="/users", method = RequestMethod.POST)
    public ResponseEntity<Object> createUser(@RequestBody User user) {
        userService.createUser(user);
        return new ResponseEntity<>("User Created Successfully", HttpStatus.CREATED);
    }

    // Get Users
    @RequestMapping(value="/users", method = RequestMethod.GET)
    public ResponseEntity<Object> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    // Delete Users
    @RequestMapping(value = "/users/{userid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteUser(@PathVariable Long userid) {
        return userService.deleteUser(userid);
    }

    // Update Users
    @RequestMapping(value="/users/{userid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateUser(@PathVariable Long userid, @RequestBody User user) {
        return userService.updateUser(userid, user);
    }

    @RequestMapping(value="/users/register", method = RequestMethod.POST)
    // This method takes a "request body as a map of key-value pairs". It also throws a "UserException" in case of any errors.
    public ResponseEntity<Object> register(@RequestBody Map<String, String> body) throws UserException {
        // This retrieves the value associated with the username key from the request body map.
        String username = body.get("username");

        // Checks if the user provided exists in the database. If existing, a prompt appears displaying "Username already exists"
        if(!userService.findByUsername(username).isEmpty()) {
            throw new UserException("Username Already Exists.");
        }
        else {
            // This retrieves the value associated with the password key from the request body map.
            String password = body.get("password");

            // This encrypts the password
            String encodedPassword = new BCryptPasswordEncoder().encode(password);

            // Instantiates the User model to create a new user
            User newUser = new User(username, encodedPassword);

            // saves it in newUser in the database
            userService.createUser(newUser);

            // A message prompt will display "user registered successfully"
            return new ResponseEntity<>("User Registered Successfully", HttpStatus.CREATED);
        }


    }
}
