package com.zuitt.discussion.config;

import com.zuitt.discussion.services.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private JwtAuthenticate jwtAuthenticate;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());

    }
    // @Bean annotation is used to make a special method. The method creates an object that can be used in different part of the application
    @Bean
    public JwtAuthenticate jwtAuthenticationEntryPointBean() throws Exception{
        return new JwtAuthenticate();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();

    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        // It enables the CORS and disabling CSRF protection
        httpSecurity.cors().and().csrf().disable()

                // Configures the authorization rules
                .authorizeRequests()

                // It allows the following endpoint to be accessible without requiring any authentication
                .antMatchers("/authenticate").permitAll()
                .antMatchers("/users/register").permitAll()

                // It allows the endpoint to be accessible via GET method
                .antMatchers(HttpMethod.GET, "/posts").permitAll()
                .antMatchers(HttpMethod.GET, "/courses").permitAll()

                // It allows unrestricted access to requests made to any URL in the application
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()

                // It requires proper authentication in order to be accessed
                .anyRequest().authenticated().and()

                // It configures the authentication for handling authentication failures
                .exceptionHandling().authenticationEntryPoint(jwtAuthenticate).and()

                // It is used to specify that the application should not create or use sessions
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // It is used to add a custom filter
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

    }
}
